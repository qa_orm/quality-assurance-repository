Scenarios-122
verify that below fields are available under approved partner tab for account object
Account name
owner
lead source
lead status
Reapplication status
Not interested
Not qualified reason code

Verify that system Admin user is able to update account name under approved partner record type
Verify that Partner loyality user is able to update account name under approved partner record type
when user profile other than amin/partner loyality user tries to change account name below meassage appears  'You do not have the appropriate permissions to change the account name.'

verify that when any user tries to change ownership from field 'owner' under approved partner tab it displays 'You do not have the appropriate permissions to change lead ownership'.
verify that once partner lead source has been set , user can't change it ,if tires below message displays:Once the Partner Lead Source has been set, you cannot change this.
verify that below error message displays to user if he tries to change status from field 'Partner lead status' :Displayed Error Message: You do not have the appropriate permissions to change the Partner Lead to this status.
verify that user can only change reapplication status to App Package Sent to inactive Partner, Application Package Uploaded.
verify that if user tries to change status other than App Package Sent to inactive Partner and Application Package Uploaded error message displays:You do not have the appropriate permissions to change the reapplication status to this status. You CAN use these statuses: App Package Sent to inactive Partner, Application Package Uploaded.
verify that below error message displays when user do not select any Not interested Reason code.:'You must choose a Not Interested Reason Code.'
verify that below error message displays when user do not select any Not Qualified Reason code.:'You must choose a Not Qualified Reason Code.'
01204007600
7983132201
8130404588


Scenarios-123
verify that below fields are available under contact object:
Cell phone
Individual Lead
Owner
Lead Source, Trade Show
Role, ContactNMLS
Lead Source, How did you hear about us
Verify that phone number field accepts only numbers and only upto 10 digits.
Verify that below message appears if user enters values other than numeric and more than 10 digits :'Only numbers are allowed in this field. Please input 10 numbers.'
Verify that System admin is able to change lead source while updating the record
verify that once lead source has been set user should not be allowed to change the lead source
Verify that only users belonging to profiles(Business development,Marketing partner loyalty,System admin and sales leader) able to change the owner of the individual lead.
Verify that when users other than above profile tries to change the owner error message:You do not have the appropriate permissions to change lead ownership displays
Verify that field 'Trade show' becomes mandatory if user selects "Trade Show" option for field 'lead source'.message displays if left blank is You must choose a Tradeshow'.
Verify that  If user selects Loan Officer in the Role, Contact NMLS should be mandatory. error message displays if left blank :'You must enter the Contact NMLS'.
Verify that  If user selects "QLMS Website" as lead source, How did you hear about us becomes mandatory. error message displays if left blannk :'You must choose "How did you hear about us?"

Verify that If user selects any of "1. Other, 2. Purchased List, 3. Webinar" as Lead Source, then Source name should be mandatory.Error message displayed: You must enter a source name


