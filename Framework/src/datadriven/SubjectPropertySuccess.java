package datadriven;

import java.util.List;
import org.testng.asserts.*;
import java.util.concurrent.TimeUnit;

import java.io.IOException;


import org.openqa.selenium.support.ui.Select;
import org.apache.xmlbeans.impl.tool.Extension.Param;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import utility.Read_XLS;
import utility.SuiteUtility;
import TestSuiteBase.SuiteBase;

public class SubjectPropertySuccess extends SuiteBase {
	
	Read_XLS FilePath = null;
	String SheetName = null;
	String TestCaseName = null;	
	String ToRunColumnNameTestCase = null;
	String ToRunColumnNameTestData = null;
	String TestDataToRun[]=null;
	String ExpectedResultInt=null;
	String ActualResult="test";
	static boolean TestCasePass=true;
	static int DataSet=-1;	
	static boolean Testskip=false;
	static boolean Testfail=false;
	static boolean success=true;
	SoftAssert s_assert =null;
	SuiteBase Sb= new SuiteBase();
@BeforeTest
public void checkCaseToRun() throws IOException{
	//Called init() function from SuiteBase class to Initialize .xls Files
	init();	
	//To set SuiteOne.xls file's path In FilePath Variable.
	FilePath = TestCaseListExcelTwo;		

	TestCaseName = this.getClass().getSimpleName();	
	//SheetName to check CaseToRun flag against test case.
	SheetName = "TestCasesList";
	//Name of column In TestCasesList Excel sheet.
	ToRunColumnNameTestCase = "CaseToRun";
	//Name of column In Test Case Data sheets.
	ToRunColumnNameTestData = "DataToRun";
	
	//To check test case's CaseToRun = Y or N In related excel sheet.
	//If CaseToRun = N or blank, Test case will skip execution. Else It will be executed.
	if(!SuiteUtility.checkToRunUtility(FilePath, SheetName,ToRunColumnNameTestCase,TestCaseName)){
		//To report result as skip for test cases In TestCasesList sheet.
		SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "SKIP");
		//To throw skip exception for this test case.
		throw new SkipException(TestCaseName+"'s CaseToRun Flag Is 'N' Or Blank. So Skipping Execution Of "+TestCaseName);
	}	
	//To retrieve DataToRun flags of all data set lines from related test data sheet.
	TestDataToRun = SuiteUtility.checkToRunUtilityOfData(FilePath, TestCaseName, ToRunColumnNameTestData);
}


@Test(dataProvider="getSubPropData")
public void DSubjpropery(String DataCol1,String DataCol2,String DataCol3,String DataCol4,String DataCol5,String DataCol6,String DataCol7,
		String DataCol8,String DataCol9,String DataCol10,
		 String ExpectedResult){
	
	DataSet++;
	
	//Created object of testng SoftAssert class.
	s_assert = new SoftAssert();
	
	//If found DataToRun = "N" for data set then execution will be skipped for that data set.
	if(!TestDataToRun[DataSet].equalsIgnoreCase("Y")){	
		//If DataToRun = "N", Set Testskip=true.
		Testskip=true;
		throw new SkipException("DataToRun for row number "+DataSet+" Is No Or Blank. So Skipping Its Execution.");
	}
	System.out.println("starting subject property test");
	WebElement E2= driver.findElement(By.xpath(SuiteBase.Object.getProperty("SubAddress")));
	E2.sendKeys(DataCol1);
	WebElement E3= driver.findElement(By.xpath(SuiteBase.Object.getProperty("SubCity")));
	E3.sendKeys(DataCol2);
	WebElement E4= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Subzip")));
	E4.sendKeys(DataCol3);
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	WebElement E1= driver.findElement(By.xpath(SuiteBase.Object.getProperty("residence")));
	jse.executeScript("arguments[0].scrollIntoView()", E1);
	WebElement E5= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Sublegal")));
	E5.sendKeys(DataCol4);
	WebElement E6= driver.findElement(By.xpath(SuiteBase.Object.getProperty("ResidenceType")));
	E6.click();
	WebElement E7= driver.findElement(By.xpath(SuiteBase.Object.getProperty("noofunit")));
	E7.sendKeys(DataCol5);
	WebElement E8= driver.findElement(By.xpath(SuiteBase.Object.getProperty("yearbuilt")));
	E8.sendKeys(DataCol6);
	driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
	Actions actions = new Actions(driver);
	WebElement E11= driver.findElement(By.xpath(SuiteBase.Object.getProperty("properyheldname")));
	jse.executeScript("arguments[0].scrollIntoView()", E11);
	WebElement E9= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Propertyheldas")));
	//jse.executeScript("arguments[0].scrollIntoView()", E9);
	//E9.sendKeys(DataCol7);
	E9.click();
	
	WebElement E10= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Miscellaneous")));
	jse.executeScript("arguments[0].scrollIntoView()", E10);
	//WebElement E11= driver.findElement(By.xpath(SuiteBase.Object.getProperty("properyheldname")));
	E11.sendKeys(DataCol7);
	WebElement E12= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Titleheldas")));
	E12.click();
	WebElement E13= driver.findElement(By.xpath(SuiteBase.Object.getProperty("taxmonthly")));
	E13.sendKeys(DataCol8);
	WebElement E14= driver.findElement(By.xpath(SuiteBase.Object.getProperty("hazardmonthly")));
	E14.sendKeys(DataCol9);
	E10.sendKeys(DataCol10);
	
	
	getElementByXPath("Savencontinue").click();
	
	System.out.println(ExpectedResult);
	ActualResult="h3";
	System.out.println(ActualResult);
	
	
	
	
	 if(!(ActualResult.contains(ExpectedResult))){
			Add_Log.info(TestCaseName+" : Actual Value "+ActualResult+" and expected value "+ExpectedResult+" Not match. Test data set failed.");
			//If expected and actual results not match, Set flag Testfail=true.
			Testfail=true;	
			//If result Is fail then test failure will be captured Inside s_assert object reference.
			//This soft assertion will not stop your test execution.
			s_assert.assertEquals(ActualResult, ExpectedResult, ActualResult+" And "+ExpectedResult+" Not Match");
			
		}
		
		if(Testfail){
			//At last, test data assertion failure will be reported In testNG reports and It will mark your test data, test case and test suite as fail.
			s_assert.assertAll();		
		}     }


@AfterMethod()
public void reporterDataResults(){		
	if(Testskip){
		//If found Testskip = true, Result will be reported as SKIP against data set line In excel sheet.
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Pass/Fail/Skip", DataSet+1, "SKIP");
	}
	else if(Testfail){
		//To make object reference null after reporting In report.
		s_assert = null;
		//Set TestCasePass = false to report test case as fail In excel sheet.
		TestCasePass=false;	
		//If found Testfail = true, Result will be reported as FAIL against data set line In excel sheet.
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Pass/Fail/Skip", DataSet+1, "FAIL");			
	}
	else{
		//If found Testskip = false and Testfail = false, Result will be reported as PASS against data set line In excel sheet.
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Pass/Fail/Skip", DataSet+1, "PASS");
	}
	//At last make both flags as false for next data set.
	Testskip=false;
	Testfail=false;
}
@AfterTest
public void closeBrowser(){
	if(TestCasePass){
		System.out.println("this should run");
		SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "PASS");
	}
	else{
		SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "FAIL");
		
	}
	//driver.quit();
}

@DataProvider
public Object[][] getSubPropData()
{
	//i stands for no of times tstcase should run
	//j stands for no of parameters it should send in one go
	return SuiteUtility.GetTestDataUtility(FilePath, TestCaseName);
}	
}


