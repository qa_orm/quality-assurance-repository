package datadriven;

import java.util.concurrent.TimeUnit;

import java.io.IOException;

import org.apache.xmlbeans.impl.tool.Extension.Param;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.*;
import org.testng.asserts.SoftAssert;

import utility.Read_XLS;
import utility.SuiteUtility;
import TestSuiteBase.SuiteBase;

public class SanL extends SuiteBase {
	
	Read_XLS FilePath = null;
	String SheetName = null;
	String TestCaseName = null;	
	String ToRunColumnNameTestCase = null;
	String ToRunColumnNameTestData = null;
	String TestDataToRun[]=null;
	String ExpectedResultInt=null;
	String ActualResult="test";
	static boolean TestCasePass=true;
	static int DataSet=-1;	
	static boolean Testskip=false;
	static boolean Testfail=false;
	SoftAssert s_assert =null;
	SuiteBase Sb= new SuiteBase();
@BeforeTest
public void checkCaseToRun() throws IOException{
	//Called init() function from SuiteBase class to Initialize .xls Files
	init();	
	//To set SuiteOne.xls file's path In FilePath Variable.
	FilePath = TestCaseListExcelTwo;		

	TestCaseName = this.getClass().getSimpleName();	
	//SheetName to check CaseToRun flag against test case.
	SheetName = "TestCasesList";
	//Name of column In TestCasesList Excel sheet.
	ToRunColumnNameTestCase = "CaseToRun";
	//Name of column In Test Case Data sheets.
	ToRunColumnNameTestData = "DataToRun";
	
	//To check test case's CaseToRun = Y or N In related excel sheet.
	//If CaseToRun = N or blank, Test case will skip execution. Else It will be executed.
	if(!SuiteUtility.checkToRunUtility(FilePath, SheetName,ToRunColumnNameTestCase,TestCaseName)){
		//To report result as skip for test cases In TestCasesList sheet.
		SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "SKIP");
		//To throw skip exception for this test case.
		throw new SkipException(TestCaseName+"'s CaseToRun Flag Is 'N' Or Blank. So Skipping Execution Of "+TestCaseName);
	}	
	//To retrieve DataToRun flags of all data set lines from related test data sheet.
	TestDataToRun = SuiteUtility.checkToRunUtilityOfData(FilePath, TestCaseName, ToRunColumnNameTestData);
}

@Test(dataProvider="sanl")
public void BSaNL(String ExpectedResult) throws InterruptedException 
{
	DataSet++;
s_assert = new SoftAssert();
	
	//If found DataToRun = "N" for data set then execution will be skipped for that data set.
	if(!TestDataToRun[DataSet].equalsIgnoreCase("Y")){	
		Add_Log.info(TestCaseName+" : DataToRun = N for data set line "+(DataSet+1)+" So skipping Its execution.");
		//If DataToRun = "N", Set Testskip=true.
		Testskip=true;
		throw new SkipException("DataToRun for row number "+DataSet+" Is No Or Blank. So Skipping Its Execution.");
	}
	
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	Actions actions = new Actions(driver);
	driver.findElement(By.xpath("//a[@title='Start a New Loan']")).click();
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	WebElement datet=driver.findElement(By.xpath("//div[@class='form-element uiInput uiInputDate uiInput--default uiInput--input uiInput--datetime']/input"));
	jse.executeScript("arguments[0].scrollIntoView()", datet);
	datet.click();
	//driver.findElement(By.xpath("//div/form/div/div[@class='form-element uiInput uiInputDate uiInput--default uiInput--input uiInput--datetime']/a")).click();
	WebElement date=driver.findElement(By.xpath("//div[@class='scroller slds-datepicker']//div[@class='uiDatePickerGrid--default uiDatePickerGrid']//table[@class='calGrid']//tbody//td[@aria-selected='true']"));
	//jse.executeScript("arguments[0].scrollIntoView()", date);
	jse.executeScript("scroll(0, 250);");

	date.click();
	
	
	WebElement checkbx1=driver.findElement(By.xpath("//div[@class='slds-form-element']//input[@data-interactive-lib-uid='4']"));
	//Actions actions = new Actions(driver);
	actions.moveToElement(checkbx1).perform();
	//driver.findElement(By.xpath("//form[@class='slds-form--inline']//div[@class='slds-form-element__control']")).click();
	//driver.findElement(By.xpath("//div[@class='scroller slds-datepicker']//div[@class='uiDatePickerGrid--default uiDatePickerGrid']//table[@class='calGrid']//tbody//td[@aria-selected='true']")).click();
	checkbx1.click();
	WebElement checkbx2=driver.findElement(By.xpath("//div[@class='slds-form-element']//input[@data-interactive-lib-uid='5']"));
	//JavascriptExecutor jse = (JavascriptExecutor)driver;

	jse.executeScript("arguments[0].scrollIntoView()", checkbx2); 
	//actions.moveToElement(checkbx2).perform();
	checkbx2.click();
	driver.findElement(By.xpath("//button[contains(.,'Start Application')]")).click();
	
     System.out.println("navigating to loan page");
     driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
     WebElement E1= driver.findElement(By.xpath(SuiteBase.Object.getProperty("revloan")));
     ActualResult=E1.getText();
     System.out.println(ActualResult);
   	
     if(!(ActualResult.equalsIgnoreCase(ExpectedResult))){
			Add_Log.info(TestCaseName+" : Actual Value "+ActualResult+" and expected value "+ExpectedResult+" Not match. Test data set failed.");
			//If expected and actual results not match, Set flag Testfail=true.
			Testfail=true;	
			//If result Is fail then test failure will be captured Inside s_assert object reference.
			//This soft assertion will not stop your test execution.
			s_assert.assertEquals(ActualResult, ExpectedResult, ActualResult+" And "+ExpectedResult+" Not Match");
		}
		
		if(Testfail){
			//At last, test data assertion failure will be reported In testNG reports and It will mark your test data, test case and test suite as fail.
			s_assert.assertAll();		
		}
	
}

@AfterMethod()
public void reporterDataResults(){		
	if(Testskip){
		Add_Log.info(TestCaseName+" : Reporting test data set line "+(DataSet+1)+" as SKIP In excel.");
		//If found Testskip = true, Result will be reported as SKIP against data set line In excel sheet.
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Pass/Fail/Skip", DataSet+1, "SKIP");
	}
	else if(Testfail){
		Add_Log.info(TestCaseName+" : Reporting test data set line "+(DataSet+1)+" as FAIL In excel.");
		//To make object reference null after reporting In report.
		s_assert = null;
		//Set TestCasePass = false to report test case as fail In excel sheet.
		TestCasePass=false;	
		//If found Testfail = true, Result will be reported as FAIL against data set line In excel sheet.
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Pass/Fail/Skip", DataSet+1, "FAIL");			
	}else{
		Add_Log.info(TestCaseName+" : Reporting test data set line "+(DataSet+1)+" as PASS In excel.");
		//If found Testskip = false and Testfail = false, Result will be reported as PASS against data set line In excel sheet.
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Pass/Fail/Skip", DataSet+1, "PASS");
	}
	//At last make both flags as false for next data set.
	Testskip=false;
	Testfail=false;
}

@AfterTest
public void closeBrowser(){
	if(TestCasePass){
		Add_Log.info(TestCaseName+" : Reporting test case as PASS In excel.");
		SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "PASS");
	}
	else{
		Add_Log.info(TestCaseName+" : Reporting test case as FAIL In excel.");
		SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "FAIL");
		
	}		
}

@DataProvider
public Object[][] sanl()
{
	//i stands for no of times tstcase should run
	//j stands for no of parameters it should send in one go
	return SuiteUtility.GetTestDataUtility(FilePath, TestCaseName);
}	
}


