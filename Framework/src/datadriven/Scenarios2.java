package datadriven;

import java.util.Currency;
import java.util.List;
import org.testng.asserts.*;
import java.util.concurrent.TimeUnit;

import java.io.IOException;
import java.text.DecimalFormat;

import org.openqa.selenium.support.ui.Select;
import org.apache.xmlbeans.impl.tool.Extension.Param;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import utility.Read_XLS;
import utility.SuiteUtility;
import TestSuiteBase.SuiteBase;

public class Scenarios2 extends SuiteBase {
	
	Read_XLS FilePath = null;
	String SheetName = null;
	String TestCaseName = null;	
	String ToRunColumnNameTestCase = null;
	String ToRunColumnNameTestData = null;
	String TestDataToRun[]=null;
	String ExpectedResultInt=null;
	String ActualResult="test";
	static boolean marginnotmatch=false;
	static boolean TestCasePass=true;
	static int DataSet=-1;	
	static boolean Testskip=false;
	static boolean Testfail=false;
	static boolean success=true;
	SoftAssert s_assert =null;
	SuiteBase Sb= new SuiteBase();
@BeforeTest
public void checkCaseToRun() throws IOException{
	//Called init() function from SuiteBase class to Initialize .xls Files
	init();	
	//To set SuiteOne.xls file's path In FilePath Variable.
	FilePath = TestCaseListExcelTwo;		

	TestCaseName = this.getClass().getSimpleName();	
	//SheetName to check CaseToRun flag against test case.
	SheetName = "TestCasesList";
	//Name of column In TestCasesList Excel sheet.
	ToRunColumnNameTestCase = "CaseToRun";
	//Name of column In Test Case Data sheets.
	ToRunColumnNameTestData = "DataToRun";
	
	//To check test case's CaseToRun = Y or N In related excel sheet.
	//If CaseToRun = N or blank, Test case will skip execution. Else It will be executed.
	if(!SuiteUtility.checkToRunUtility(FilePath, SheetName,ToRunColumnNameTestCase,TestCaseName)){
		//To report result as skip for test cases In TestCasesList sheet.
		SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "SKIP");
		//To throw skip exception for this test case.
		throw new SkipException(TestCaseName+"'s CaseToRun Flag Is 'N' Or Blank. So Skipping Execution Of "+TestCaseName);
	}	
	//To retrieve DataToRun flags of all data set lines from related test data sheet.
	TestDataToRun = SuiteUtility.checkToRunUtilityOfData(FilePath, TestCaseName, ToRunColumnNameTestData);
}



@Test(dataProvider="getscenarios")
public void Cloan(String EHV,String Balance,String interest,String payment,String firstname,String lastname,
		String address,String zip,String phone,String S1,String S2,String S3,String S4,String S5,String S6,String S7,
		String S8,String S9,String S10,String S11,String S12,String S13,String S14,
		String S15,String S16,String S17,String S18,String S19,String S20,String S21,
		String S22,String S23,String S24,String S25,String S26,String S27,String S28,
		String S29,String S30,String S31,String S32,String S33,String S34,String S35,
		
		String S36,String S37,String S38,String S39,String S40,String S41,String S42,
		 String ExpectedResult,String S43,String S44,String S45,String S46,String S47,String S48,String S49) throws InterruptedException{
	
DataSet++;
	
	//Created object of testng SoftAssert class.
	s_assert = new SoftAssert();
	
	//If found DataToRun = "N" for data set then execution will be skipped for that data set.
	if(!TestDataToRun[DataSet].equalsIgnoreCase("Y")){	
		Add_Log.info(TestCaseName+" : DataToRun = N for data set line "+(DataSet+1)+" So skipping Its execution.");
		//If DataToRun = "N", Set Testskip=true.
		Testskip=true;
		throw new SkipException("DataToRun for row number "+DataSet+" Is No Or Blank. So Skipping Its Execution.");
	}
	 getElementByXPath("click_scenario").click();
	 Thread.sleep(100);
	 
	 
	 Actions actions = new Actions(driver);
	 
	WebElement E1= driver.findElement(By.xpath(SuiteBase.Object.getProperty("scn_dob")));
	actions.moveToElement(E1).perform();
	E1.sendKeys("01/01/1941");
	WebElement E2= driver.findElement(By.xpath(SuiteBase.Object.getProperty("scn_EHV")));
	E2.sendKeys(EHV);
	//getElementByXPath("loanoriginationfee").click();
	//getElementByXPath("ApplicationtakenBy").click();
	WebElement E3= driver.findElement(By.xpath(SuiteBase.Object.getProperty("scn_CMB")));
	E3.sendKeys(Balance);
	
	WebElement E4= driver.findElement(By.xpath(SuiteBase.Object.getProperty("scn_interest")));
	E4.sendKeys(interest);
	//System.out.println(E2.getText());
	//System.out.println(E3.getText());
	WebElement E5= driver.findElement(By.xpath(SuiteBase.Object.getProperty("scn_monthlypayment")));
	E5.sendKeys(payment);
	WebElement E6= driver.findElement(By.xpath(SuiteBase.Object.getProperty("btn_cal")));
	
	E6.click();
	WebElement E7= driver.findElement(By.xpath(SuiteBase.Object.getProperty("margin_1")));
	JavascriptExecutor jse = (JavascriptExecutor)driver;

	jse.executeScript("arguments[0].scrollIntoView()", E7); 
	actions.moveToElement(E7).perform();
	String margin1=getElementByXPath("margin_1").getText();
	String prinamnt1=getElementByXPath("prinamnt1").getText();
	String loc1=getElementByXPath("loc1").getText();
	String uti1=getElementByXPath("uti1").getText();
	String comp1=getElementByXPath("comp1").getText();
	String amnt1=getElementByXPath("amnt1").getText();
	String amt1=trimfunc(prinamnt1);
	String loc11=trimfunc(loc1);
	String amnt11=trimfunc(amnt1);
	uti1=trimfunc(uti1);
	comp1=trimfunc(comp1);
	if(!amt1.contains(S2)||!margin1.contains(S1)||!loc11.contains(S3)||!uti1.contains(S4)||!comp1.contains(S5)||!amnt11.contains(S6)) {
		marginnotmatch=true;
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Data not match for margin 2.0", DataSet+1, "Data not match for margin 2.0  ");
	}
	else {
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Data not match for margin 2.0", DataSet+1, "Data Match  ");
	}
	WebElement E8= driver.findElement(By.xpath(SuiteBase.Object.getProperty("margin_2")));
	jse.executeScript("arguments[0].scrollIntoView()", E8); 
	actions.moveToElement(E8).perform();
	String margin_2=getElementByXPath("margin_2").getText();
	String prinamnt2=getElementByXPath("prinamnt2").getText();
	String loc2=getElementByXPath("loc2").getText();
	String uti2=getElementByXPath("uti2").getText();
	String comp2=getElementByXPath("comp2").getText();
	String amnt2=getElementByXPath("amnt2").getText();
	margin_2=trimfunc(margin_2);
	prinamnt2=trimfunc(prinamnt2);
	loc2=trimfunc(loc2);
	uti2=trimfunc(uti2);
	comp2=trimfunc(comp2);
	amnt2=trimfunc(amnt2);
	if(!prinamnt2.contains(S8)||!margin_2.contains(S7)||!loc2.contains(S9)||!uti2.contains(S10)||!comp2.contains(S11)||!amnt2.contains(S12))
	{
		marginnotmatch=true;
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Data not match for margin 2.125", DataSet+1, "Data not match for margin 2.125  ");
	}
	else {
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Data not match for margin 2.125", DataSet+1, "Data Match ");
	}
	WebElement E9= driver.findElement(By.xpath(SuiteBase.Object.getProperty("margin_3")));
	jse.executeScript("arguments[0].scrollIntoView()", E9); 
	actions.moveToElement(E9).perform();
	String margin_3=getElementByXPath("margin_3").getText();
	String prinamnt3=getElementByXPath("prinamnt3").getText();
	String loc3=getElementByXPath("loc3").getText();
	String uti3=getElementByXPath("uti3").getText();
	String comp3=getElementByXPath("comp3").getText();
	String amnt3=getElementByXPath("amnt3").getText();
	margin_3=trimfunc(margin_3);
	prinamnt3=trimfunc(prinamnt3);
	loc3=trimfunc(loc3);
	uti3=trimfunc(uti3);
	comp3=trimfunc(comp3);
	amnt3=trimfunc(amnt3);
	if(!margin_3.contains(S13)||!prinamnt3.contains(S14)||!loc3.contains(S15)||!uti3.contains(S16)||!comp3.contains(S17)||!amnt3.contains(S18))
	{
		marginnotmatch=true;
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Data not match for margin 2.250", DataSet+1, "Data not match for margin 2.250  ");
	}
	else {
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Data not match for margin 2.250", DataSet+1, "Data Match ");	
	}
	WebElement E10= driver.findElement(By.xpath(SuiteBase.Object.getProperty("margin_4")));
	jse.executeScript("arguments[0].scrollIntoView()", E10); 
	actions.moveToElement(E10).perform();
	String margin_4=getElementByXPath("margin_4").getText();
	String prinamnt4=getElementByXPath("prinamnt4").getText();
	String loc4=getElementByXPath("loc4").getText();
	String uti4=getElementByXPath("uti4").getText();
	String comp4=getElementByXPath("comp4").getText();
	String amnt4=getElementByXPath("amnt4").getText();
	margin_4=trimfunc(margin_4);
	prinamnt4=trimfunc(prinamnt4);
	loc4=trimfunc(loc4);
	uti4=trimfunc(uti4);
	comp4=trimfunc(comp4);
	amnt4=trimfunc(amnt4);
	if(!margin_4.contains(S19)||!prinamnt4.contains(S20)||!loc4.contains(S21)||!uti4.contains(S22)||!comp4.contains(S23)||!amnt4.contains(S24))
	{
		marginnotmatch=true;
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Data not match for margin 2.375", DataSet+1, "Data not match for margin 2.375  ");
	}
	else {
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Data not match for margin 2.375", DataSet+1, "Data Match");
	}
	WebElement E11= driver.findElement(By.xpath(SuiteBase.Object.getProperty("margin_5")));
	jse.executeScript("arguments[0].scrollIntoView()", E11); 
	actions.moveToElement(E11).perform();
	String margin_5=getElementByXPath("margin_5").getText();
	String prinamnt5=getElementByXPath("prinamnt5").getText();
	String loc5=getElementByXPath("loc5").getText();
	String uti5=getElementByXPath("uti5").getText();
	String comp5=getElementByXPath("comp5").getText();
	String amnt5=getElementByXPath("amnt5").getText();
	margin_5=trimfunc(margin_5);
	prinamnt5=trimfunc(prinamnt5);
	loc5=trimfunc(loc5);
	uti5=trimfunc(uti5);
	comp5=trimfunc(comp5);
	amnt5=trimfunc(amnt5);
	if(!margin_5.contains(S25)||!prinamnt5.contains(S26)||!loc5.contains(S27)||!uti5.contains(S28)||!comp5.contains(S29)||!amnt5.contains(S30))
	{
		marginnotmatch=true;
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Data not match for margin 2.500", DataSet+1, "Data not match for margin 2.500 ");
	}
	else {
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Data not match for margin 2.500", DataSet+1, "Data Match");
	}
	String margin6=getElementByXPath("margin_6").getText();
	String prinamnt6=getElementByXPath("prinamnt6").getText();
	String loc6=getElementByXPath("loc6").getText();
	String uti6=getElementByXPath("uti6").getText();
	String comp6=getElementByXPath("comp6").getText();
	String amnt6=getElementByXPath("amnt6").getText();
	margin6=trimfunc(margin6);
	prinamnt6=trimfunc(prinamnt6);
	loc6=trimfunc(loc6);
	uti6=trimfunc(uti6);
	comp6=trimfunc(comp6);
	amnt6=trimfunc(amnt6);
	if(!margin6.contains(S31)||!prinamnt6.contains(S32)||!loc6.contains(S33)||!uti6.contains(S34)||!comp6.contains(S35)||!amnt6.contains(S36))
	{
		marginnotmatch=true;
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Data not match for margin 2.625", DataSet+1, "Data not match for margin 2.625  ");
	}
	else {
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Data not match for margin 2.625", DataSet+1, "Data Match ");
	}
	String margin7=getElementByXPath("margin_7").getText();
	String prinamnt7=getElementByXPath("prinamnt7").getText();
	String loc7=getElementByXPath("loc7").getText();
	String uti7=getElementByXPath("uti7").getText();
	String comp7=getElementByXPath("comp7").getText();
	String amnt7=getElementByXPath("amnt7").getText();
	margin7=trimfunc(margin7);
	prinamnt7=trimfunc(prinamnt7);
	loc7=trimfunc(loc7);
	uti7=trimfunc(uti7);
	comp7=trimfunc(comp7);
	amnt7=trimfunc(amnt7);
	if(!margin7.contains(S37)||!prinamnt7.contains(S38)||!loc7.contains(S39)||!uti7.contains(S40)||!comp7.contains(S41)||!amnt7.contains(S42))
	{
		marginnotmatch=true;
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Data not match for margin 2.750", DataSet+1, "Data not match for margin 2.750  ");
	}
	else {
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Data not match for margin 2.750", DataSet+1, "Data Match  ");
	}
	//driver.wait(1000);
	
	
	WebElement clickmargin= driver.findElement(By.xpath(SuiteBase.Object.getProperty("margin_5")));
	clickmargin.click();
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	//========================================================================================================
	
	//actions.moveToElement(savebtn).perform();
	WebElement E81= driver.findElement(By.xpath(SuiteBase.Object.getProperty("cl_firstname")));
	E81.sendKeys(firstname);
	WebElement E91= driver.findElement(By.xpath(SuiteBase.Object.getProperty("cl_lastname")));
	E91.sendKeys(lastname);
	WebElement E101= driver.findElement(By.xpath(SuiteBase.Object.getProperty("streetadd")));
	E101.sendKeys(address);
	//===========================================================================================================
	WebElement E111= driver.findElement(By.xpath(SuiteBase.Object.getProperty("cl_zip")));
	E111.sendKeys(zip);
	WebElement E12= driver.findElement(By.xpath(SuiteBase.Object.getProperty("cl_phonenumb")));
	E12.sendKeys(phone);
	WebElement savebtn= driver.findElement(By.xpath(SuiteBase.Object.getProperty("btn_savescenario")));
	jse.executeScript("arguments[0].scrollIntoView()", savebtn);
	savebtn.click();
	WebElement confirmation= driver.findElement(By.xpath(SuiteBase.Object.getProperty("confirmation_text")));
	String ActualResult=confirmation.getText();
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	WebElement Requestbtn= driver.findElement(By.xpath(SuiteBase.Object.getProperty("btn_requestscenario")));
	Requestbtn.click();
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	WebElement Newscenario= driver.findElement(By.xpath(SuiteBase.Object.getProperty("btn_newscenario")));
	Newscenario.click();
	System.out.println(ExpectedResult);
	
	System.out.println(ActualResult);
	
	  if(!(ActualResult.contains(ExpectedResult))||marginnotmatch){
			Add_Log.info(TestCaseName+" : Actual Value "+ActualResult+" and expected value "+ExpectedResult+" Not match. Test data set failed.");
			//If expected and actual results not match, Set flag Testfail=true.
			Testfail=true;	
			//If result Is fail then test failure will be captured Inside s_assert object reference.
			//This soft assertion will not stop your test execution.
			s_assert.assertEquals(ActualResult, ExpectedResult, ActualResult+" And "+ExpectedResult+" Not Match");
			s_assert.assertEquals(marginnotmatch, false);
		
		}
	  else {
		  
	  }
		if(Testfail){
			//At last, test data assertion failure will be reported In testNG reports and It will mark your test data, test case and test suite as fail.
			s_assert.assertAll();		
		}
	
		
		driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
     }

public String trimfunc(String inputstring)
{
	String newstring=inputstring.replace("$", "");
	String OutputString1=newstring.replace(",", "");
	String OutputString=OutputString1.replace(".", "");
	OutputString=OutputString.replace("%","");
	return OutputString;
	
}
@AfterMethod()
public void reporterDataResults1(){		
	if(Testskip){
		Add_Log.info(TestCaseName+" : Reporting test data set line "+(DataSet+1)+" as SKIP In excel.");
		//If found Testskip = true, Result will be reported as SKIP against data set line In excel sheet.
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Pass/Fail/Skip", DataSet+1, "SKIP");
	}
	else if(Testfail){
		Add_Log.info(TestCaseName+" : Reporting test data set line "+(DataSet+1)+" as FAIL In excel.");
		//To make object reference null after reporting In report.
		s_assert = null;
		//Set TestCasePass = false to report test case as fail In excel sheet.
		TestCasePass=false;	
		//If found Testfail = true, Result will be reported as FAIL against data set line In excel sheet.
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Pass/Fail/Skip", DataSet+1, "FAIL");	
			
	}else{
		
		Add_Log.info(TestCaseName+" : Reporting test data set line "+(DataSet+1)+" as PASS In excel.");
		//If found Testskip = false and Testfail = false, Result will be reported as PASS against data set line In excel sheet.
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Pass/Fail/Skip", DataSet+1, "PASS");
	}
	//At last make both flags as false for next data set.
	Testskip=false;
	Testfail=false;
}

@AfterTest
public void closeBrowser1(){
	if(TestCasePass){
		System.out.println("this should run");
		SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "PASS");
	}
	else{
		SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "FAIL");
		
	}
	//driver.quit();
}

@DataProvider
public Object[][] getscenarios()
{
	//i stands for no of times tstcase should run
	//j stands for no of parameters it should send in one go
	return SuiteUtility.GetTestDataUtility(FilePath, TestCaseName);
}	
}


