package datadriven;

import java.util.List;
import org.testng.asserts.*;
import java.util.concurrent.TimeUnit;

import java.io.IOException;


import org.openqa.selenium.support.ui.Select;
import org.apache.xmlbeans.impl.tool.Extension.Param;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import utility.Read_XLS;
import utility.SuiteUtility;
import TestSuiteBase.SuiteBase;

public class TClientSuccess extends SuiteBase {
	
	Read_XLS FilePath = null;
	String SheetName = null;
	String TestCaseName = null;	
	String ToRunColumnNameTestCase = null;
	String ToRunColumnNameTestData = null;
	String TestDataToRun[]=null;
	String ExpectedResultInt=null;
	String ActualResult="test";
	String loanid=null;
	static boolean TestCasePass=true;
	static int DataSet=-1;	
	static int clientnoiterator=0;
	static boolean Testskip=false;
	static boolean Testfail=false;
	static boolean success=true;
	SoftAssert s_assert =null;
	SuiteBase Sb= new SuiteBase();
	int noofclient=0;
	int loanno=0;
@BeforeTest
public void checkCaseToRun() throws IOException{
	//Called init() function from SuiteBase class to Initialize .xls Files
	init();	
	//To set SuiteOne.xls file's path In FilePath Variable.
	FilePath = TestCaseListExcelTwo;		

	TestCaseName = this.getClass().getSimpleName();	
	//SheetName to check CaseToRun flag against test case.
	SheetName = "TestCasesList";
	//Name of column In TestCasesList Excel sheet.
	ToRunColumnNameTestCase = "CaseToRun";
	//Name of column In Test Case Data sheets.
	ToRunColumnNameTestData = "DataToRun";
	
	//To check test case's CaseToRun = Y or N In related excel sheet.
	//If CaseToRun = N or blank, Test case will skip execution. Else It will be executed.
	if(!SuiteUtility.checkToRunUtility(FilePath, SheetName,ToRunColumnNameTestCase,TestCaseName)){
		//To report result as skip for test cases In TestCasesList sheet.
		SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "SKIP");
		//To throw skip exception for this test case.
		throw new SkipException(TestCaseName+"'s CaseToRun Flag Is 'N' Or Blank. So Skipping Execution Of "+TestCaseName);
	}	
	//To retrieve DataToRun flags of all data set lines from related test data sheet.
	TestDataToRun = SuiteUtility.checkToRunUtilityOfData(FilePath, TestCaseName, ToRunColumnNameTestData);
}


@Test(dataProvider="getclientdata")
public void Eclient(String DataCol1,String DataCol2,String DataCol3,String DataCol4,String DataCol5,String DataCol6,String DataCol7,
		String DataCol8,String DataCol9,String DataCol10,String DataCol11,String DataCol12,String DataCol13,String DataCol14,String DataCol15,String DataCol16,String DataCol17,
		String DataCol18, String ExpectedResult) throws InterruptedException{
	
DataSet++;
	
	//Created object of testng SoftAssert class.
	s_assert = new SoftAssert();
	
	//If found DataToRun = "N" for data set then execution will be skipped for that data set.
	if(!TestDataToRun[DataSet].equalsIgnoreCase("Y")){	
		Add_Log.info(TestCaseName+" : DataToRun = N for data set line "+(DataSet+1)+" So skipping Its execution.");
		//If DataToRun = "N", Set Testskip=true.
		Testskip=true;
		throw new SkipException("DataToRun for row number "+DataSet+" Is No Or Blank. So Skipping Its Execution.");
	}
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	System.out.println("starting create client test");
	//driver.manage().timeouts().implicitlyWait(1000,TimeUnit.SECONDS) ;
	WebElement E35=driver.findElement(By.xpath(SuiteBase.Object.getProperty("Addclient_btn")));
	jse.executeScript("arguments[0].scrollIntoView()", E35);
	E35.click();
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	WebElement E1= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Firstname")));
	E1.sendKeys(DataCol1);
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	WebElement E2= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Middlename")));
	E2.sendKeys(DataCol2);
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	WebElement E3= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Lastname")));
	E3.sendKeys(DataCol3);
	String Clientnamefull=DataCol3+","+DataCol1+" "+DataCol2;
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	
	WebElement E4= driver.findElement(By.xpath(SuiteBase.Object.getProperty("noofyerscurrentaddress")));
	jse.executeScript("arguments[0].scrollIntoView()", E4);
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	WebElement E5= driver.findElement(By.xpath(SuiteBase.Object.getProperty("currentaddress")));
	E5.sendKeys(DataCol4);
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	WebElement E6= driver.findElement(By.xpath(SuiteBase.Object.getProperty("currentcity")));
	E6.sendKeys(DataCol5);
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	WebElement E7= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Currentstate")));
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	E7.click();
	WebElement E8= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Currentzip")));
	E8.sendKeys(DataCol6);
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	E4.sendKeys(DataCol7);
	
	//driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	Actions actions = new Actions(driver);
	WebElement E9= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Ssn")));
	jse.executeScript("arguments[0].scrollIntoView()", E9);
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	
	WebElement E10= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Homephone")));
	//jse.executeScript("arguments[0].scrollIntoView()", E9);
	//E9.sendKeys(DataCol7);
	E10.sendKeys(DataCol8);
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	WebElement E11= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Mobileph")));
	//jse.executeScript("arguments[0].scrollIntoView()", E10);
	//WebElement E11= driver.findElement(By.xpath(SuiteBase.Object.getProperty("properyheldname")));
	E11.sendKeys(DataCol9);
	WebElement E31= driver.findElement(By.xpath(SuiteBase.Object.getProperty("dob")));
	E31.click();
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	WebElement E32= driver.findElement(By.xpath(SuiteBase.Object.getProperty("dob_year")));
	E32.click();
	driver.findElement(By.xpath(SuiteBase.Object.getProperty("dob_select"))).click();
	E9.sendKeys(DataCol10);
	WebElement E12= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Mailcity")));
	jse.executeScript("arguments[0].scrollIntoView()", E12);
	WebElement E13= driver.findElement(By.xpath(SuiteBase.Object.getProperty("maritalsttus")));
	E13.click();
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	/*WebElement E14= driver.findElement(By.xpath(SuiteBase.Object.getProperty("residence2")));
	E14.click();
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	WebElement E15= driver.findElement(By.xpath(SuiteBase.Object.getProperty("propertytype")));
	E15.click();*/
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	WebElement E16= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Mailingadd")));
	E16.sendKeys(DataCol11);
	E12.sendKeys(DataCol12);
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	WebElement E17= driver.findElement(By.xpath(SuiteBase.Object.getProperty("spusedetail")));
	jse.executeScript("arguments[0].scrollIntoView()", E17);
	WebElement E18= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Mailstate")));
	E18.click();
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	WebElement E19= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Mailzip")));
	E19.sendKeys(DataCol13);
	WebElement E20= driver.findElement(By.xpath(SuiteBase.Object.getProperty("nonborwingspouse")));
	E20.click();
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	WebElement E21= driver.findElement(By.xpath(SuiteBase.Object.getProperty("nonbspouseadd")));
	jse.executeScript("arguments[0].scrollIntoView()", E21);
	WebElement E22= driver.findElement(By.xpath(SuiteBase.Object.getProperty("spousename")));
	E22.sendKeys(DataCol14);
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	WebElement E30= driver.findElement(By.xpath(SuiteBase.Object.getProperty("spousephone")));
	E30.sendKeys(DataCol15);	
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	WebElement E23= driver.findElement(By.xpath(SuiteBase.Object.getProperty("spousedob")));
	E23.click();
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	WebElement E24= driver.findElement(By.xpath(SuiteBase.Object.getProperty("spousedobpick")));
	E24.click();
	WebElement E25= driver.findElement(By.xpath(SuiteBase.Object.getProperty("nonbrelationship")));
	E25.click();
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	WebElement E26= driver.findElement(By.xpath(SuiteBase.Object.getProperty("nonbspouseadd")));
	E26.sendKeys(DataCol16);
	WebElement E27= driver.findElement(By.xpath(SuiteBase.Object.getProperty("nonbspousecity")));
	E27.sendKeys(DataCol17);
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	
	WebElement E28= driver.findElement(By.xpath(SuiteBase.Object.getProperty("spousezip")));
	jse.executeScript("arguments[0].scrollIntoView()", E28);
	WebElement E29= driver.findElement(By.xpath(SuiteBase.Object.getProperty("spousestate")));
	E29.click();
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	E28.sendKeys(DataCol18);
	Thread.sleep(10000);
	WebElement E33= driver.findElement(By.xpath(SuiteBase.Object.getProperty("loanid")));
	String fullloanid=E33.getText();
	loanid=fullloanid.substring(10);
	
		
	System.out.println("Loan id is   "+loanid);
	Thread.sleep(10000);
	getElementByXPath("savebtn").click();
	Thread.sleep(10000);
	
	//getElementByXPath("nxtbtn1").click();
	noofclient++;
	SuiteUtility.WriteResultUtility1(FilePath, "Employment", "noofclient", 1, noofclient);
	SuiteUtility.WriteResultUtility(FilePath, "Employment", "loanid", 1, loanid);
	SuiteUtility.WriteResultUtility(FilePath, "Income", "client name", clientnoiterator+1, Clientnamefull);
	SuiteUtility.WriteResultUtility(FilePath, "Income", "client name", clientnoiterator+2, Clientnamefull);
	SuiteUtility.WriteResultUtility(FilePath, "Income", "client name", clientnoiterator+3, Clientnamefull);
	SuiteUtility.WriteResultUtility(FilePath, "Income", "client name", clientnoiterator+4, Clientnamefull);
	SuiteUtility.WriteResultUtility(FilePath, "Employment", "client name", DataSet+1, Clientnamefull);
	clientnoiterator=4;
	
	System.out.println(ExpectedResult);
	ActualResult="h3";
	System.out.println(ActualResult);
	
	  if(!(ActualResult.contains(ExpectedResult))){
			Add_Log.info(TestCaseName+" : Actual Value "+ActualResult+" and expected value "+ExpectedResult+" Not match. Test data set failed.");
			//If expected and actual results not match, Set flag Testfail=true.
			Testfail=true;	
			//If result Is fail then test failure will be captured Inside s_assert object reference.
			//This soft assertion will not stop your test execution.
			s_assert.assertEquals(ActualResult, ExpectedResult, ActualResult+" And "+ExpectedResult+" Not Match");
		
		}
		
		if(Testfail){
			//At last, test data assertion failure will be reported In testNG reports and It will mark your test data, test case and test suite as fail.
			s_assert.assertAll();		
		}
		

     }
public int loanno()
{
	WebElement E33= driver.findElement(By.xpath(SuiteBase.Object.getProperty("loanid")));
	String fullloanid=E33.getText();
	loanid=fullloanid.substring(10);
	loanno=Integer.parseInt(loanid);
	return loanno;
	
}

@AfterMethod()
public void reporterDataResults(){		
	if(Testskip){
		//If found Testskip = true, Result will be reported as SKIP against data set line In excel sheet.
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Pass/Fail/Skip", DataSet+1, "SKIP");
	}
	else if(Testfail){
		//To make object reference null after reporting In report.
		s_assert = null;
		//Set TestCasePass = false to report test case as fail In excel sheet.
		TestCasePass=false;	
		//If found Testfail = true, Result will be reported as FAIL against data set line In excel sheet.
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Pass/Fail/Skip", DataSet+1, "FAIL");			
	}
	else{
		//If found Testskip = false and Testfail = false, Result will be reported as PASS against data set line In excel sheet.
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Pass/Fail/Skip", DataSet+1, "PASS");
	}
	//At last make both flags as false for next data set.
	Testskip=false;
	Testfail=false;
}
@AfterTest
public void closeBrowser(){
	getElementByXPath("nxtbtn1").click();
	System.out.println("no of clients created"+noofclient);
	if(TestCasePass){
		System.out.println("this should run");
		SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "PASS");
	}
	else{
		SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "FAIL");
		
	}
	//driver.quit();
}

@DataProvider
public Object[][] getclientdata()
{
	//i stands for no of times tstcase should run
	//j stands for no of parameters it should send in one go
	return SuiteUtility.GetTestDataUtility(FilePath, TestCaseName);
}	
}


