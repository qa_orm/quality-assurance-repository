package datadriven;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
 
import java.util.List;
import org.testng.asserts.*;
import java.util.concurrent.TimeUnit;

import java.io.IOException;


import org.openqa.selenium.support.ui.Select;
import org.apache.xmlbeans.impl.tool.Extension.Param;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import utility.Read_XLS;
import utility.SuiteUtility;
import TestSuiteBase.SuiteBase;

public class Income extends SuiteBase {
	
	Read_XLS FilePath = null;
	String SheetName = null;
	String TestCaseName = null;	
	String ToRunColumnNameTestCase = null;
	String ToRunColumnNameTestData = null;
	String TestDataToRun[]=null;
	String ExpectedResultInt=null;
	String ActualResult="test";
	static boolean TestCasePass=true;
	static int DataSet=-1;	
	static boolean Testskip=false;
	static boolean Testfail=false;
	static boolean success=true;
	SoftAssert s_assert =null;
	TClientSuccess Sb= new TClientSuccess();
@BeforeTest
public void checkCaseToRun() throws IOException{
	//Called init() function from SuiteBase class to Initialize .xls Files
	init();	
	//To set SuiteOne.xls file's path In FilePath Variable.
	FilePath = TestCaseListExcelTwo;		

	TestCaseName = this.getClass().getSimpleName();	
	//SheetName to check CaseToRun flag against test case.
	SheetName = "TestCasesList";
	//Name of column In TestCasesList Excel sheet.
	ToRunColumnNameTestCase = "CaseToRun";
	//Name of column In Test Case Data sheets.
	ToRunColumnNameTestData = "DataToRun";
	
	//To check test case's CaseToRun = Y or N In related excel sheet.
	//If CaseToRun = N or blank, Test case will skip execution. Else It will be executed.
	if(!SuiteUtility.checkToRunUtility(FilePath, SheetName,ToRunColumnNameTestCase,TestCaseName)){
		//To report result as skip for test cases In TestCasesList sheet.
		SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "SKIP");
		//To throw skip exception for this test case.
		throw new SkipException(TestCaseName+"'s CaseToRun Flag Is 'N' Or Blank. So Skipping Execution Of "+TestCaseName);
	}	
	//To retrieve DataToRun flags of all data set lines from related test data sheet.
	TestDataToRun = SuiteUtility.checkToRunUtilityOfData(FilePath, TestCaseName, ToRunColumnNameTestData);
}


@Test(dataProvider="Empdata")
public void Aincomplete(String noofclient,String loanid,String Clientname,String ExpectedResult) throws Exception{
	// ExcelApiTest eat = new ExcelApiTest("C:\\Users\\gxadmin\\Framework\\src\\Excel files\\OrmTest.xls ");


	JavascriptExecutor jse = (JavascriptExecutor)driver;
	driver.findElement(By.xpath("//a[@title='Start a New Loan']")).click();
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	System.out.println("navigate to start a new loan page and open incomplete application");
	driver.findElement(By.xpath("//button[contains(.,\"Incomplete Applications\")]/span")).click();
	
	driver.findElement(By.xpath("//div[@class='slds-grid'][contains(., \"" +loanid+"\")]//a[contains(.,\"Complete Registration\")]")).click();
	Thread.sleep(10000);
	
	getElementByXPath("nxtbtn1").click();
}
@Test(dataProvider="Incomedata")
public void Income1(String IncomeType,String Source,String Value,String clientname,
String ExpectedResult)throws InterruptedException{
DataSet++;
	
	//Created object of testng SoftAssert class.
	s_assert = new SoftAssert();
	
	//If found DataToRun = "N" for data set then execution will be skipped for that data set.
	if(!TestDataToRun[DataSet].equalsIgnoreCase("Y")){	
		Add_Log.info(TestCaseName+" : DataToRun = N for data set line "+(DataSet+1)+" So skipping Its execution.");
		//If DataToRun = "N", Set Testskip=true.
		Testskip=true;
		throw new SkipException("DataToRun for row number "+DataSet+" Is No Or Blank. So Skipping Its Execution.");
	}
	Select dropdown = new Select(driver.findElement(By.xpath("//div[@class='slds-form-element']//select")));
	dropdown.getOptions();
	List<WebElement> noofclients=dropdown.getOptions();
	int i=noofclients.size();  
	System.out.println(i);
	dropdown.selectByIndex(1);
	
	driver.findElement(By.xpath("//tr[contains(.,\""+IncomeType+"\")]//td/a[@xpath='"+DataSet+1+"']")).click();
	WebElement E1= driver.findElement(By.xpath(SuiteBase.Object.getProperty("income_Sourcename")));
	E1.clear();
	E1.sendKeys(Source);
	WebElement E2= driver.findElement(By.xpath(SuiteBase.Object.getProperty("income_Incomevalue")));
	E2.clear();
	E2.sendKeys(Value);
	
	driver.findElement(By.xpath(SuiteBase.Object.getProperty("income_savebutton"))).click();
	//driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	
	
	//driver.manage().timeouts().implicitlyWait(1000,TimeUnit.SECONDS) ;
	
	
	
	System.out.println(ExpectedResult);
	ActualResult="h3";
	System.out.println(ActualResult);
	
	  if(!(ActualResult.contains(ExpectedResult))){
			Add_Log.info(TestCaseName+" : Actual Value "+ActualResult+" and expected value "+ExpectedResult+" Not match. Test data set failed.");
			//If expected and actual results not match, Set flag Testfail=true.
			Testfail=true;	
			//If result Is fail then test failure will be captured Inside s_assert object reference.
			//This soft assertion will not stop your test execution.
			s_assert.assertEquals(ActualResult, ExpectedResult, ActualResult+" And "+ExpectedResult+" Not Match");
		
		}
		
		if(Testfail){
			//At last, test data assertion failure will be reported In testNG reports and It will mark your test data, test case and test suite as fail.
			s_assert.assertAll();		
		}
	

     }


@AfterMethod()
public void reporterDataResults(){		
	if(Testskip){
		//If found Testskip = true, Result will be reported as SKIP against data set line In excel sheet.
		SuiteUtility.WriteResultUtility(FilePath, "Income", "Pass/Fail/Skip", DataSet+1, "SKIP");
	}
	else if(Testfail){
		//To make object reference null after reporting In report.
		s_assert = null;
		//Set TestCasePass = false to report test case as fail In excel sheet.
		TestCasePass=false;	
		//If found Testfail = true, Result will be reported as FAIL against data set line In excel sheet.
		SuiteUtility.WriteResultUtility(FilePath, "Income", "Pass/Fail/Skip", DataSet+1, "FAIL");			
	}
	else{
		//If found Testskip = false and Testfail = false, Result will be reported as PASS against data set line In excel sheet.
		SuiteUtility.WriteResultUtility(FilePath, "Income", "Pass/Fail/Skip", DataSet+1, "PASS");
	}
	//At last make both flags as false for next data set.
	Testskip=false;
	Testfail=false;
}
@AfterTest
public void closeBrowser(){
	//getElementByXPath("nxtbtn1").click();
	if(TestCasePass){
		System.out.println("this should run");
		SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "PASS");
	}
	else{
		SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "FAIL");
		
	}
	//driver.quit();
}

@DataProvider
public Object[][] Empdata()
{
	//i stands for no of times tstcase should run
	//j stands for no of parameters it should send in one go
	return SuiteUtility.GetTestDataUtility(FilePath, "Employment");
}	

@DataProvider
public Object[][] Incomedata()
{
	//i stands for no of times tstcase should run
	//j stands for no of parameters it should send in one go
	return SuiteUtility.GetTestDataUtility(FilePath, TestCaseName);
}
}


