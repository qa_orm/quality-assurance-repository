package datadriven;

import java.util.List;
import org.testng.asserts.*;
import java.util.concurrent.TimeUnit;

import java.io.IOException;


import org.openqa.selenium.support.ui.Select;
import org.apache.xmlbeans.impl.tool.Extension.Param;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import utility.Read_XLS;
import utility.SuiteUtility;
import TestSuiteBase.SuiteBase;

public class Newloanerror extends SuiteBase {
	
	Read_XLS FilePath = null;
	String SheetName = null;
	String TestCaseName = null;	
	String ToRunColumnNameTestCase = null;
	String ToRunColumnNameTestData = null;
	String TestDataToRun[]=null;
	String ExpectedResultInt=null;
	String ActualResult="test";
	static boolean TestCasePass=true;
	static int DataSet=-1;	
	static boolean Testskip=false;
	static boolean Testfail=false;
	static boolean success=true;
	SoftAssert s_assert =null;
	SuiteBase Sb= new SuiteBase();
@BeforeTest
public void checkCaseToRun() throws IOException{
	//Called init() function from SuiteBase class to Initialize .xls Files
	init();	
	//To set SuiteOne.xls file's path In FilePath Variable.
	FilePath = TestCaseListExcelTwo;		

	TestCaseName = this.getClass().getSimpleName();	
	//SheetName to check CaseToRun flag against test case.
	SheetName = "TestCasesList";
	//Name of column In TestCasesList Excel sheet.
	ToRunColumnNameTestCase = "CaseToRun";
	//Name of column In Test Case Data sheets.
	ToRunColumnNameTestData = "DataToRun";
	
	//To check test case's CaseToRun = Y or N In related excel sheet.
	//If CaseToRun = N or blank, Test case will skip execution. Else It will be executed.
	if(!SuiteUtility.checkToRunUtility(FilePath, SheetName,ToRunColumnNameTestCase,TestCaseName)){
		//To report result as skip for test cases In TestCasesList sheet.
		SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "SKIP");
		//To throw skip exception for this test case.
		throw new SkipException(TestCaseName+"'s CaseToRun Flag Is 'N' Or Blank. So Skipping Execution Of "+TestCaseName);
	}	
	//To retrieve DataToRun flags of all data set lines from related test data sheet.
	TestDataToRun = SuiteUtility.checkToRunUtilityOfData(FilePath, TestCaseName, ToRunColumnNameTestData);
}



@Test(dataProvider="getLoanData")
public void Cloan(String DataCol1,String DataCol2,String DataCol3,String DataCol4,String DataCol5,String DataCol6,String DataCol7,
		String DataCol8,String DataCol9,String DataCol10,String 
		DataCol11,String DataCol12,String DataCol13,String DataCol14,String DataCol15,
		String DataCol16,String DataCol17,String DataCol18,String DataCol19,String DataCol20,String DataCol21,
		 String ExpectedResult){
	
DataSet++;
	
	//Created object of testng SoftAssert class.
	s_assert = new SoftAssert();
	
	//If found DataToRun = "N" for data set then execution will be skipped for that data set.
	if(!TestDataToRun[DataSet].equalsIgnoreCase("Y")){	
		Add_Log.info(TestCaseName+" : DataToRun = N for data set line "+(DataSet+1)+" So skipping Its execution.");
		//If DataToRun = "N", Set Testskip=true.
		Testskip=true;
		throw new SkipException("DataToRun for row number "+DataSet+" Is No Or Blank. So Skipping Its Execution.");
	}
	 getElementByXPath("MortgageAppliedfor").click();
	 getElementByXPath("Purposeofloan").click();
	 getElementByXPath("Selectedloanpayment").click();
	 Actions actions = new Actions(driver);
	 
	WebElement E1= driver.findElement(By.xpath(SuiteBase.Object.getProperty("EstimateAppraisedvalue")));
	actions.moveToElement(E1).perform();
	E1.sendKeys(DataCol1);
	driver.findElement(By.xpath(SuiteBase.Object.getProperty("loanmargin"))).click();
	WebElement E6= driver.findElement(By.xpath(SuiteBase.Object.getProperty("applicationDate")));
	actions.moveToElement(E6).perform();
	//getElementByXPath("loanoriginationfee").click();
	//getElementByXPath("ApplicationtakenBy").click();
	WebElement E2= driver.findElement(By.xpath(SuiteBase.Object.getProperty("loanoriginationfee")));
	E2.click();
	
	WebElement E3= driver.findElement(By.xpath(SuiteBase.Object.getProperty("ApplicationtakenBy")));
	E3.click();
	//System.out.println(E2.getText());
	//System.out.println(E3.getText());
	/*WebElement E4= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Originationfirstname")));
	E4.sendKeys(DataCol2);
	WebElement E5= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Originationlastname")));
	
	E5.sendKeys(DataCol3);*/
	//WebElement E7= driver.findElement(By.xpath(SuiteBase.Object.getProperty("applicationdatevalue")));
	//actions.moveToElement(E6).perform();
	
	//E7.sendKeys("01/01/2017");
	/*WebElement E8= driver.findElement(By.xpath(SuiteBase.Object.getProperty("loanoriginationcompstate")));
	actions.moveToElement(E8).perform();
	//========================================================================================================
	WebElement E9= driver.findElement(By.xpath(SuiteBase.Object.getProperty("loanorigninatonphoneno")));
	E9.sendKeys(DataCol5);
	WebElement E10= driver.findElement(By.xpath(SuiteBase.Object.getProperty("loanoriginatiocompnyname")));
	E10.sendKeys(DataCol6);
	WebElement E11= driver.findElement(By.xpath(SuiteBase.Object.getProperty("loanoriginatiocompnyaddrss")));
	E11.sendKeys(DataCol7);
	WebElement E12= driver.findElement(By.xpath(SuiteBase.Object.getProperty("loanoriginatiocompnycity")));
	E12.sendKeys(DataCol8);
	//===========================================================================================================
	WebElement E13= driver.findElement(By.xpath(SuiteBase.Object.getProperty("childunderage6")));
	actions.moveToElement(E13).perform();
	WebElement E14= driver.findElement(By.xpath(SuiteBase.Object.getProperty("loanoriginatiocompnystate")));
	E14.click();
	WebElement E15= driver.findElement(By.xpath(SuiteBase.Object.getProperty("loanoriginatiocompnyzip")));
	E15.sendKeys(DataCol10);*/
	/*WebElement E16= driver.findElement(By.xpath(SuiteBase.Object.getProperty("loanoriginationidentifier")));
	E16.sendKeys(DataCol11);
	WebElement E17= driver.findElement(By.xpath(SuiteBase.Object.getProperty("loanoriginationcmpnyidentifier")));
	E17.sendKeys(DataCol12);*/
	WebElement E18= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Householdmember")));
	E18.click();
	//==========================================================================================================
	//WebElement E19= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Alternatesection")));
	//actions.moveToElement(E19).perform();
	
	//E18.sendKeys(DataCol13);
	WebElement E20= driver.findElement(By.xpath(SuiteBase.Object.getProperty("childunderage6value")));
	E20.click();
	WebElement E21= driver.findElement(By.xpath(SuiteBase.Object.getProperty("IsthrAPoa")));
	E21.click();
	WebElement E22= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Poaname")));
	E22.sendKeys(DataCol16);
	WebElement E23= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Squrfootage")));
	E23.sendKeys(DataCol17);
	WebElement E28= driver.findElement(By.xpath(SuiteBase.Object.getProperty("SavenContinue")));
	actions.moveToElement(E28).perform();
	WebElement E24= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Altrnatename")));
	E24.sendKeys(DataCol18);
	WebElement E25= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Altrnataddress")));
	E25.sendKeys(DataCol19);
	
	WebElement E26= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Altrnatphno")));
	E26.sendKeys(DataCol20);
	WebElement E27= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Relationship")));
	E27.click();
	//WebElement E28= driver.findElement(By.xpath(SuiteBase.Object.getProperty("SavenContinue")));
	JavascriptExecutor jse = (JavascriptExecutor)driver;
    
	jse.executeScript("arguments[0].scrollIntoView()", E28); 
	E28.click();
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	WebElement E29= driver.findElement(By.xpath(SuiteBase.Object.getProperty("Error")));
	System.out.println(ExpectedResult);
	ActualResult=E29.getText();
	System.out.println(ActualResult);
	
	  if(!(ActualResult.contains(ExpectedResult))){
			Add_Log.info(TestCaseName+" : Actual Value "+ActualResult+" and expected value "+ExpectedResult+" Not match. Test data set failed.");
			//If expected and actual results not match, Set flag Testfail=true.
			Testfail=true;	
			//If result Is fail then test failure will be captured Inside s_assert object reference.
			//This soft assertion will not stop your test execution.
			s_assert.assertEquals(ActualResult, ExpectedResult, ActualResult+" And "+ExpectedResult+" Not Match");
			E1.clear();
			E22.clear();E23.clear();E24.clear();E25.clear();E26.clear();
		}
	  else {
		  E1.clear();
			E22.clear();E23.clear();E24.clear();E25.clear();E26.clear();
	  }
		if(Testfail){
			//At last, test data assertion failure will be reported In testNG reports and It will mark your test data, test case and test suite as fail.
			s_assert.assertAll();		
		}
	
		
		driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
     }


@AfterMethod()
public void reporterDataResults1(){		
	if(Testskip){
		Add_Log.info(TestCaseName+" : Reporting test data set line "+(DataSet+1)+" as SKIP In excel.");
		//If found Testskip = true, Result will be reported as SKIP against data set line In excel sheet.
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Pass/Fail/Skip", DataSet+1, "SKIP");
	}
	else if(Testfail){
		Add_Log.info(TestCaseName+" : Reporting test data set line "+(DataSet+1)+" as FAIL In excel.");
		//To make object reference null after reporting In report.
		s_assert = null;
		//Set TestCasePass = false to report test case as fail In excel sheet.
		TestCasePass=false;	
		//If found Testfail = true, Result will be reported as FAIL against data set line In excel sheet.
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Pass/Fail/Skip", DataSet+1, "FAIL");			
	}else{
		Add_Log.info(TestCaseName+" : Reporting test data set line "+(DataSet+1)+" as PASS In excel.");
		//If found Testskip = false and Testfail = false, Result will be reported as PASS against data set line In excel sheet.
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Pass/Fail/Skip", DataSet+1, "PASS");
	}
	//At last make both flags as false for next data set.
	Testskip=false;
	Testfail=false;
}

@AfterTest
public void closeBrowser1(){
	if(TestCasePass){
		System.out.println("this should run");
		SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "PASS");
	}
	else{
		SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "FAIL");
		
	}
	//driver.quit();
}

@DataProvider
public Object[][] getLoanData()
{
	//i stands for no of times tstcase should run
	//j stands for no of parameters it should send in one go
	return SuiteUtility.GetTestDataUtility(FilePath, TestCaseName);
}	
}


