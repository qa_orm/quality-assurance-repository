package datadriven;

import java.util.List;
import org.testng.asserts.*;
import java.util.concurrent.TimeUnit;

import java.io.IOException;


import org.openqa.selenium.support.ui.Select;
import org.apache.xmlbeans.impl.tool.Extension.Param;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import utility.Read_XLS;
import utility.SuiteUtility;
import TestSuiteBase.SuiteBase;

public class Scenarios extends SuiteBase {
	
	Read_XLS FilePath = null;
	String SheetName = null;
	String TestCaseName = null;	
	String ToRunColumnNameTestCase = null;
	String ToRunColumnNameTestData = null;
	String TestDataToRun[]=null;
	String ExpectedResultInt=null;
	String ActualResult="test";
	static boolean TestCasePass=true;
	static int DataSet=-1;	
	static boolean Testskip=false;
	static boolean Testfail=false;
	static boolean success=true;
	SoftAssert s_assert =null;
	SuiteBase Sb= new SuiteBase();
@BeforeTest
public void checkCaseToRun() throws IOException{
	//Called init() function from SuiteBase class to Initialize .xls Files
	init();	
	//To set SuiteOne.xls file's path In FilePath Variable.
	FilePath = TestCaseListExcelTwo;		

	TestCaseName = this.getClass().getSimpleName();	
	//SheetName to check CaseToRun flag against test case.
	SheetName = "TestCasesList";
	//Name of column In TestCasesList Excel sheet.
	ToRunColumnNameTestCase = "CaseToRun";
	//Name of column In Test Case Data sheets.
	ToRunColumnNameTestData = "DataToRun";
	
	//To check test case's CaseToRun = Y or N In related excel sheet.
	//If CaseToRun = N or blank, Test case will skip execution. Else It will be executed.
	if(!SuiteUtility.checkToRunUtility(FilePath, SheetName,ToRunColumnNameTestCase,TestCaseName)){
		//To report result as skip for test cases In TestCasesList sheet.
		SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "SKIP");
		//To throw skip exception for this test case.
		throw new SkipException(TestCaseName+"'s CaseToRun Flag Is 'N' Or Blank. So Skipping Execution Of "+TestCaseName);
	}	
	//To retrieve DataToRun flags of all data set lines from related test data sheet.
	TestDataToRun = SuiteUtility.checkToRunUtilityOfData(FilePath, TestCaseName, ToRunColumnNameTestData);
}



@Test(dataProvider="getscenarios")
public void Cloan(String EHV,String Balance,String interest,String payment,String firstname,String lastname,
		String address,String zip,String phone,String ExpectedResult) throws InterruptedException{
	
DataSet++;
	
	//Created object of testng SoftAssert class.
	s_assert = new SoftAssert();
	
	//If found DataToRun = "N" for data set then execution will be skipped for that data set.
	if(!TestDataToRun[DataSet].equalsIgnoreCase("Y")){	
		Add_Log.info(TestCaseName+" : DataToRun = N for data set line "+(DataSet+1)+" So skipping Its execution.");
		//If DataToRun = "N", Set Testskip=true.
		Testskip=true;
		throw new SkipException("DataToRun for row number "+DataSet+" Is No Or Blank. So Skipping Its Execution.");
	}
	 getElementByXPath("click_scenario").click();
	 Thread.sleep(100);
	 
	 
	 Actions actions = new Actions(driver);
	 
	WebElement E1= driver.findElement(By.xpath(SuiteBase.Object.getProperty("scn_dob")));
	actions.moveToElement(E1).perform();
	E1.sendKeys("01/01/1941");
	WebElement E2= driver.findElement(By.xpath(SuiteBase.Object.getProperty("scn_EHV")));
	E2.sendKeys(EHV);
	//getElementByXPath("loanoriginationfee").click();
	//getElementByXPath("ApplicationtakenBy").click();
	WebElement E3= driver.findElement(By.xpath(SuiteBase.Object.getProperty("scn_CMB")));
	E3.sendKeys(Balance);
	
	WebElement E4= driver.findElement(By.xpath(SuiteBase.Object.getProperty("scn_interest")));
	E4.sendKeys(interest);
	//System.out.println(E2.getText());
	//System.out.println(E3.getText());
	WebElement E5= driver.findElement(By.xpath(SuiteBase.Object.getProperty("scn_monthlypayment")));
	E5.sendKeys(payment);
	WebElement E6= driver.findElement(By.xpath(SuiteBase.Object.getProperty("btn_cal")));
	
	E6.click();
	//driver.wait(1000);
	String margin1=getElementByXPath("margin1").getText();
	String prinamnt1=getElementByXPath("prinamnt1").getText();
	String loc1=getElementByXPath("loc1").getText();
	String uti1=getElementByXPath("uti1").getText();
	String comp1=getElementByXPath("comp1").getText();
	String amnt1=getElementByXPath("amnt1").getText();
	
	//s_assert.assertEquals(margin1, S1, " Not Match");
	//s_assert.assertEquals(prinamnt1, S2, " Not Match");
	
	
	String margin2=getElementByXPath("margin2").getText();
	String prinamnt2=getElementByXPath("prinamnt2").getText();
	String loc2=getElementByXPath("loc2").getText();
	String uti2=getElementByXPath("uti2").getText();
	String comp2=getElementByXPath("comp2").getText();
	String amnt2=getElementByXPath("amnt2").getText();
	
	
	String margin3=getElementByXPath("margin3").getText();
	String prinamnt3=getElementByXPath("prinamnt3").getText();
	String loc3=getElementByXPath("loc3").getText();
	String uti3=getElementByXPath("uti3").getText();
	String comp3=getElementByXPath("comp3").getText();
	String amnt3=getElementByXPath("amnt3").getText();

	String margin4=getElementByXPath("margin4").getText();
	String prinamnt4=getElementByXPath("prinamnt4").getText();
	String loc4=getElementByXPath("loc4").getText();
	String uti4=getElementByXPath("uti4").getText();
	String comp4=getElementByXPath("comp4").getText();
	String amnt4=getElementByXPath("amnt4").getText();
	
	String margin5=getElementByXPath("margin5").getText();
	String prinamnt5=getElementByXPath("prinamnt5").getText();
	String loc5=getElementByXPath("loc5").getText();
	String uti5=getElementByXPath("uti5").getText();
	String comp5=getElementByXPath("comp5").getText();
	String amnt5=getElementByXPath("amnt5").getText();
	
	String margin6=getElementByXPath("margin6").getText();
	String prinamnt6=getElementByXPath("prinamnt6").getText();
	String loc6=getElementByXPath("loc6").getText();
	String uti6=getElementByXPath("uti6").getText();
	String comp6=getElementByXPath("comp6").getText();
	String amnt6=getElementByXPath("amnt6").getText();
	
	String margin7=getElementByXPath("margin7").getText();
	String prinamnt7=getElementByXPath("prinamnt7").getText();
	String loc7=getElementByXPath("loc7").getText();
	String uti7=getElementByXPath("uti7").getText();
	String comp7=getElementByXPath("comp7").getText();
	String amnt7=getElementByXPath("amnt7").getText();
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	WebElement E7= driver.findElement(By.xpath(SuiteBase.Object.getProperty("lender_margin")));
	JavascriptExecutor jse = (JavascriptExecutor)driver;

	jse.executeScript("arguments[0].scrollIntoView()", E7); 
	actions.moveToElement(E7).perform();
	WebElement clickmargin= driver.findElement(By.xpath(SuiteBase.Object.getProperty("lemnargin")));
	clickmargin.click();
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	//========================================================================================================
	
	//actions.moveToElement(savebtn).perform();
	WebElement E8= driver.findElement(By.xpath(SuiteBase.Object.getProperty("cl_firstname")));
	E8.sendKeys(firstname);
	WebElement E9= driver.findElement(By.xpath(SuiteBase.Object.getProperty("cl_lastname")));
	E9.sendKeys(lastname);
	WebElement E10= driver.findElement(By.xpath(SuiteBase.Object.getProperty("streetadd")));
	E10.sendKeys(address);
	//===========================================================================================================
	WebElement E11= driver.findElement(By.xpath(SuiteBase.Object.getProperty("cl_zip")));
	E11.sendKeys(zip);
	WebElement E12= driver.findElement(By.xpath(SuiteBase.Object.getProperty("cl_phonenumb")));
	E12.sendKeys(phone);
	WebElement savebtn= driver.findElement(By.xpath(SuiteBase.Object.getProperty("btn_savescenario")));
	jse.executeScript("arguments[0].scrollIntoView()", savebtn);
	savebtn.click();
	WebElement confirmation= driver.findElement(By.xpath(SuiteBase.Object.getProperty("confirmation_text")));
	String ActualResult=confirmation.getText();
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	WebElement Requestbtn= driver.findElement(By.xpath(SuiteBase.Object.getProperty("btn_requestscenario")));
	Requestbtn.click();
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
	WebElement Newscenario= driver.findElement(By.xpath(SuiteBase.Object.getProperty("btn_newscenario")));
	Newscenario.click();
	System.out.println(ExpectedResult);
	
	System.out.println(ActualResult);
	
	  if(!(ActualResult.contains(ExpectedResult))){
			Add_Log.info(TestCaseName+" : Actual Value "+ActualResult+" and expected value "+ExpectedResult+" Not match. Test data set failed.");
			//If expected and actual results not match, Set flag Testfail=true.
			Testfail=true;	
			//If result Is fail then test failure will be captured Inside s_assert object reference.
			//This soft assertion will not stop your test execution.
			s_assert.assertEquals(ActualResult, ExpectedResult, ActualResult+" And "+ExpectedResult+" Not Match");
		
		}
	  else {
		  
	  }
		if(Testfail){
			//At last, test data assertion failure will be reported In testNG reports and It will mark your test data, test case and test suite as fail.
			s_assert.assertAll();		
		}
	
		
		driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;
     }


@AfterMethod()
public void reporterDataResults1(){		
	if(Testskip){
		Add_Log.info(TestCaseName+" : Reporting test data set line "+(DataSet+1)+" as SKIP In excel.");
		//If found Testskip = true, Result will be reported as SKIP against data set line In excel sheet.
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Pass/Fail/Skip", DataSet+1, "SKIP");
	}
	else if(Testfail){
		Add_Log.info(TestCaseName+" : Reporting test data set line "+(DataSet+1)+" as FAIL In excel.");
		//To make object reference null after reporting In report.
		s_assert = null;
		//Set TestCasePass = false to report test case as fail In excel sheet.
		TestCasePass=false;	
		//If found Testfail = true, Result will be reported as FAIL against data set line In excel sheet.
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Pass/Fail/Skip", DataSet+1, "FAIL");			
	}else{
		Add_Log.info(TestCaseName+" : Reporting test data set line "+(DataSet+1)+" as PASS In excel.");
		//If found Testskip = false and Testfail = false, Result will be reported as PASS against data set line In excel sheet.
		SuiteUtility.WriteResultUtility(FilePath, TestCaseName, "Pass/Fail/Skip", DataSet+1, "PASS");
	}
	//At last make both flags as false for next data set.
	Testskip=false;
	Testfail=false;
}

@AfterTest
public void closeBrowser1(){
	if(TestCasePass){
		System.out.println("this should run");
		SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "PASS");
	}
	else{
		SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "FAIL");
		
	}
	//driver.quit();
}

@DataProvider
public Object[][] getscenarios()
{
	//i stands for no of times tstcase should run
	//j stands for no of parameters it should send in one go
	return SuiteUtility.GetTestDataUtility(FilePath, TestCaseName);
}	
}


