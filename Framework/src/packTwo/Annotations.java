package packTwo;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Annotations {
@BeforeTest
public void Cookies()
{
	System.out.println("Execute after all test cases");
}
@BeforeMethod
public void UseridGeneration()
{
	System.out.println("This Block executes Before each method");
}
@Test(groups= {"Priority1"})
public void OpenBrowser()
{
	System.out.println("Execute Test 2");
	
	
}
@Test(groups= {"Priority2"})
public void FlightBooking()
{
	System.out.println("Execute Test 1");
}
@AfterMethod()
public void Reportadding()
{
	System.out.println("This Block executes after each method");
}
@AfterTest
public void Cookiesclose()
{
	System.out.println("Execute after all test cases");
}
}
