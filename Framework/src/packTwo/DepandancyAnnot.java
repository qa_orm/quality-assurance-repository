package packTwo;

import org.testng.annotations.Test;

public class DepandancyAnnot {
@Test
public void OpeningBroser()
{
	System.out.println("Open browser first");
}
@Test(dependsOnMethods=("OpeningBroser"),alwaysRun=true)
public void FlightBooing()
{
	System.out.println("do booking later");
}

@Test(enabled=false)	
public void Payment()
{
	System.out.println("Open browser first");
}
@Test(timeOut=5000)	
public void TimeRelated()
{
	System.out.println("execution done in 5 Sec");
}
}
